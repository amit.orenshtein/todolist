import React, {useEffect, useRef, useState} from 'react';

export const TestingApp = () => {

    return (
        <div>
                <Counter/>
        </div>
    )
};

function usePrevious<T>(value: T): T | undefined {
    const ref = useRef<T>();
    useEffect(() => {
        console.log("Use effect in previous is running! counter: " + value);
        ref.current = value;
    });
    console.log("Return in previous is running! ref.current: " + ref.current);
    return ref.current;
}

export const Counter = () => {
    const [counter, setCounter] = useState(0);
    console.log("Starting component.");
    const prev = usePrevious(counter);

    useEffect(() => {
        console.log("Use effect is running! counter: " + counter + ". previous: " + prev);
    });

    function click() {
        console.log("Advancing counter!");
        setCounter(counter + 1);
    }

    console.log("Return of component");
    return (
        <div>
            Current counter: {counter} <br />
            Previous counter: {prev}
            <br />
            <button onClick={click} >Click</button>
        </div>
    )
};