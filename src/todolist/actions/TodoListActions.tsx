import {TaskData} from "../TodoListApp";

export const TOGGLE_TODO = "TOGGLE_TODO";
export const SET_LIST = "SET_LIST";
export const ADD_TODO = "ADD_TODO";
export const DELETE_TODO = "DELETE_TODO";

interface ToggleTodoAction {
    type: typeof TOGGLE_TODO,
    id: number
}

interface SetTodoListAction {
    type: typeof SET_LIST,
    list: TaskData[]
}

interface AddTodoAction {
    type: typeof ADD_TODO,
    task: TaskData
}

interface DeleteTodoAction {
    type: typeof DELETE_TODO,
    id: number
}

export type TodoListActions = ToggleTodoAction | SetTodoListAction | AddTodoAction | DeleteTodoAction;