import {ADD_TODO, DELETE_TODO, SET_LIST, TodoListActions, TOGGLE_TODO} from "./TodoListActions";
import {TaskData} from "../TodoListApp";

export const toggleTodo = (id: number): TodoListActions => (
    {
        type: TOGGLE_TODO,
        id: id
    }
);

export const setTodoList = (list: TaskData[]): TodoListActions => (
    {
        type: SET_LIST,
        list: list
    }
);

export const addTodo = (task: TaskData): TodoListActions => (
    {
        type: ADD_TODO,
        task: task
    }
);

export const deleteTodo = (id: number): TodoListActions => (
    {
        type: DELETE_TODO,
        id: id
    }
);