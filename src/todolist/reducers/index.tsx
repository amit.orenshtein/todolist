import {combineReducers} from "redux";
import TodolistReducer from "./TodolistReducer";
import {TaskData} from "../TodoListApp";

export type StateData = {
    todoList: TaskData[]
}

const allReducers = combineReducers<StateData>({
    todoList: TodolistReducer
});

export default allReducers;