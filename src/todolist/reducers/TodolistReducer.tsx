import {TaskData} from "../TodoListApp";
import {TodoListActions} from "../actions/TodoListActions";

export default function (state: TaskData[] = [], action: TodoListActions): TaskData[] {
    switch (action.type) {
        case "SET_LIST":
            return action.list;
        case "TOGGLE_TODO":
            return state.map(todo => {
                if(todo.id === action.id)
                    todo.isDone = !todo.isDone;
                return todo
            });
        case "ADD_TODO":
            let newList = state.slice();
            newList.push(action.task);
            return newList;
        case "DELETE_TODO":
            return state.filter(task => task.id !== action.id);
        default:
            return state;
    }
}