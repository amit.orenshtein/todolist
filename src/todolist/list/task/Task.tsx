import React from 'react';
import {TaskData} from "../../TodoListApp";
import DeleteIcon from '@material-ui/icons/Delete';
import IconBtn from "../../utils/IconButton";
import {useDispatch} from "react-redux";
import {deleteTodo, toggleTodo} from "../../actions";

const Task : React.FC<{task: TaskData}> = (props) => {
    const dispatch = useDispatch();

    function toggle(): void {
        dispatch(toggleTodo(props.task.id));
    }

    function deleteTask(): void {
        dispatch(deleteTodo(props.task.id));
    }

    return (
        <div className={'taskItem' + (props.task.isDone ? " done" : "")}>
            <input className='taskCheckbox' type='checkbox' checked={props.task.isDone} onChange={toggle}/>
            {props.task.text}
            <IconBtn btnClass="deleteTask" icon={DeleteIcon} onClick={deleteTask} iconStyle={{color: "red"}} />
        </div>
    )
};

export default Task;