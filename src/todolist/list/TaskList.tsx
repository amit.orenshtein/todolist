import React from 'react';
import './TaskList.css';

import Task from "./task/Task";
import {TaskData, TaskFunction} from "../TodoListApp";

const TaskList: React.FC<{tasks: TaskData[]}> = (props) => {
    return (
        <div id='taskList'>
            {props.tasks.map(task => <Task key={task.id} task={task} />)}
        </div>
    )
};


export default TaskList;