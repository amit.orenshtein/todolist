import React from 'react';
import './IconButton.css'

const IconBtn:React.FC<{icon: React.ElementType, btnClass: string, onClick: () => void, iconStyle?: any}> = (props) => {
    return (
        <div className={"iconBtn " + props.btnClass} onClick={props.onClick}>
            <props.icon style={props.iconStyle} />
        </div>
    );
};

export default IconBtn;