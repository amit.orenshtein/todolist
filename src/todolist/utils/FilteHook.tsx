import {useEffect, useState} from 'react';

export function useFilterList <T>(list: T[], query: string, operator: (obj: T, query:string) => boolean) : T[] {
    const [filteredList, setFilteredList] = useState(list);

    useEffect(() => {
        let newList = list.slice().filter(value => operator(value, query));
        setFilteredList(newList);
    }, [query, list]);

    return filteredList;
}