import {ChangeEvent, useState} from 'react';

type InputValue = {
    value: string,
    onChange: (e: ChangeEvent<HTMLInputElement>) => void,
    setValue: (newVal: string) => void
}

export function useInputValue(initValue: string): InputValue{
    const [value, setValue] = useState(initValue);

    function changeValue(e: ChangeEvent<HTMLInputElement>) {
        setValue(e.target.value);
    }

    return {value: value, onChange: changeValue, setValue};
}