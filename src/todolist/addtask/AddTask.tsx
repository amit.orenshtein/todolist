import React, {ChangeEvent, useEffect, useState} from 'react';
import {TaskData} from "../TodoListApp";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconBtn from "../utils/IconButton";
import './AddTask.css'
import {useInputValue} from "../utils/Util";
import {useDispatch, useSelector} from "react-redux";
import {addTodo} from "../actions";
import {StateData} from "../reducers";

export const AddTask: React.FC<{}> = (props) => {
    const taskList = useSelector((state: StateData) => state.todoList);
    const dispatch = useDispatch();
    const textInput = useInputValue('');
    const [isAddable, setIsAddable] = useState(false);

    function addTask() {
        if(!isAddable)
            return;

        let newTask: TaskData = {
            id: getAvailableId(),
            text: textInput.value,
            isDone: false
        };
        dispatch(addTodo(newTask));
        textInput.setValue('');
    }

    function getAvailableId(): number {
        let newId = -1, checkId = 1;
        while (newId === -1) {
            // eslint-disable-next-line no-loop-func
            if(taskList.findIndex(task => task.id === checkId) === -1) {
                newId = checkId;
            } else {
                checkId++;
            }
        }
        return newId;
    }

    useEffect(() => {
        setIsAddable(textInput.value !== "");
    }, [textInput.value]);

    return (
        <div id="addTaskDiv">
            <input id="addTaskInput" type='text' {... textInput}/>
            <IconBtn icon={AddCircleIcon} btnClass={("addTaskBtn" + (isAddable ? "" : " disabled"))} onClick={addTask} iconStyle={{color: (isAddable ? 'green' : 'grey')}} />
        </div>
    )
};

export default AddTask;


