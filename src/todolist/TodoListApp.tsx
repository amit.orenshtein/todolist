import React, {useEffect, useState} from 'react';
import './TodoListApp.css';
import TaskList from "./list/TaskList";
import AddTask from "./addtask/AddTask";
import {useInputValue} from "./utils/Util";
import {useFilterList} from "./utils/FilteHook";
import {useDispatch, useSelector} from "react-redux";
import {StateData} from "./reducers";
import {setTodoList} from "./actions";

export type TaskData = {
    id: number,
    text: string,
    isDone: boolean
}

export type TaskFunction = (id: number) => void;

var initTaskList: TaskData[] = [
    {
        id: 1,
        text: "Go shopping",
        isDone: false
    },
    {
        id: 2,
        text: "Learn hooks",
        isDone: true
    }

];

const AppTitle: React.FC<{}> = () => {
    return (
        <div id='appTitle'>
            Todo List
        </div>
    )
};

export const  TodoListApp : React.FC<{}> = (props) => {
    const taskList = useSelector((state: StateData) => state.todoList);
    const dispatch = useDispatch();
    const searchInput = useInputValue('');
    const filteredList = useFilterList(taskList, searchInput.value,
        (obj, text) => obj.text.includes(text));

    useEffect(() => {
        dispatch(setTodoList(initTaskList));
    }, []);

    return (
        <div id='appContainer'>
            <AppTitle />
            <input id='searchInput' type='text' {...searchInput} />
            <TaskList tasks={filteredList} />
            <AddTask />
        </div>
    )
};


export default TodoListApp;